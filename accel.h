#include <Adafruit_CircuitPlayground.h>
#include <Wire.h>
#include <SPI.h>

#ifndef _ACCEL_H_
#define _ACCEL_H_

// Adjust this number for the sensitivity of the 'click' force
// this strongly depend on the range! for 16G, try 5-10
// for 8G, try 10-20. for 4G try 20-40. for 2G try 40-80
#define CLICKTHRESHHOLD 120

#define SCALING 40 // scale LIS3DH readings to correspond to ADXL345 readings

class Accel
{
public:
    void init(void);
    void readXYZ(int * x, int * y, int * z);

private:
};

#endif
