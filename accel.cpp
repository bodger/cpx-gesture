#include <Adafruit_CircuitPlayground.h>
#include "accel.h"

void
Accel::init()
{
  CircuitPlayground.begin();

  // 0 = turn off click detection & interrupt
  // 1 = single click only interrupt output
  // 2 = double click only interrupt output, detect single click
  // Adjust threshhold, higher numbers are less sensitive
  CircuitPlayground.setAccelTap(1, CLICKTHRESHHOLD);

  // have a procedure called when a tap is detected
  //attachInterrupt(digitalPinToInterrupt(CPLAY_LIS3DH_INTERRUPT), tapTime, FALLING);
}

void
Accel::readXYZ(
  int * x,
  int * y,
  int * z)
{
  CircuitPlayground.lis.read();

  *x = CircuitPlayground.lis.x / SCALING;
  *y = CircuitPlayground.lis.y / SCALING;
  *z = CircuitPlayground.lis.z / SCALING;
}

