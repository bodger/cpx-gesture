# CPX-gesture

Port of the SeeedStudio ADXL345 Gesture Recognition library to the LIS3DH on a Circuit Playground Express

[SeeedStudio original Gesture Recognition library](https://github.com/Seeed-Studio/Gesture_Recognition/blob/master/gesture.h)
