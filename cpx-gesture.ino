#include "gesture.h"

static Gesture  gesture;

void
setup(void)
{
  while (!Serial);
  
  Serial.begin(9600);

  gesture.init();
}

void
loop()
{
  int i;

  if (gesture.checkMoveStart()) {
    Serial.println("no move start");
    // failed, bail out
    return;
  }

  if (gesture.getAccelerateData()) {
    Serial.println("no accelerate data");
    // failed, bail out
    return;
  }

  if (gesture.calculateAccelerateData()) {
    Serial.println("no calculated accelerate data");
    // failed, bail out
    return;
  }

#if 0
  for (i = 0; i < SAMPLING_NUMBER; ++i) {
    Serial.print("gesture ");
    Serial.print(gesture.accelerateArray[i][0]);
    Serial.print(" ");
    Serial.println(gesture.accelerateArray[i][1]);
  }

  Serial.println();
#endif
}
